#include <iostream>
#include "MFC.h"

class CMyWinApp : public CWinApp
{
public:
	CMyWinApp::CMyWinApp()
	{
		std::cout<<"CMyWinApp Constructor\n";
	}
	CMyWinApp::~CMyWinApp()
	{
		std::cout<<"CMyWinApp Destructor \n";
	}
};

class CMyFrameWnd : public CFrameWnd
{
public:
	CMyFrameWnd::CMyFrameWnd()
	{
		std::cout<< "CMyFrameWnd Constructor\n";
	}
	CMyFrameWnd::~CMyFrameWnd()
	{
		std::cout<<"CMyFrameWnd Destructor \n";
	}
};

